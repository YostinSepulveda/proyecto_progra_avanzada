
public class Comunidad {
	
	private int numCiudadanos;
	private int promConexionFisica;
	private String enfermedad;
	private int numInfectados;
	private double probConexionFisica;

	Comunidad(){}
	public void setnumCiudadanos(int numCiudadanos) {
		this.numCiudadanos = numCiudadanos;
	}
	
	public void setpromConexionFisica(int promConexionFisica) {
		this.promConexionFisica = promConexionFisica;
	}
	
	public void setenfermedad(String enfermedad) {
		this.enfermedad=enfermedad;
	}
	
	public void setnumInfectados(int numInfectados) {
		this.numInfectados = numInfectados;
	}

	public void setprobConexionFisica(double probConexionFisica) {
		this.probConexionFisica = probConexionFisica;
	}
	
	public int getnumCiudadanos() {
		return numCiudadanos;
	}
	
	public int getpromConexionFisica() {
		return promConexionFisica;
	}
	
	public String getenfermedad() {
		return enfermedad;
	}

	public int getnumInfectados() {
		return numInfectados;
	}

	public double getprobConexionFisica() {
		return probConexionFisica;
	}
}