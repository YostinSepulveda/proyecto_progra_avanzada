import java.util.ArrayList;
import java.util.Random;

public class Enfermedad {
	
	private boolean enfermo;
	private int contador;
	private int promPasos;
	private double probInfeccion;
	
	Enfermedad(){}
	
	public void setprobInfeccion(double probInfeccion) {
		this.probInfeccion = probInfeccion;
	}
	
	public void setpromPasos(int promPasos) {
		this.promPasos = promPasos;
	}
		
	public void setenfermo(boolean enfermo) {
		this.enfermo = enfermo;
	}

	public double getProbInfeccion() {
		return probInfeccion;
	}

	public boolean isEnfermo() {
		return enfermo;
	}

}
