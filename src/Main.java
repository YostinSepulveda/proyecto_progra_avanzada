public class Main {

	public static void main(String[] args) {
		
		Enfermedad dis = new Enfermedad();
		Comunidad com = new Comunidad();
		Simulador sim = new Simulador();
		
		dis.setprobInfeccion(0.3);
		dis.setpromPasos(18);
		
		com.setnumCiudadanos(2000);
		com.setpromConexionFisica(8);
		com.setenfermedad("enfermedad");
		com.setnumInfectados(10);
		com.setprobConexionFisica(0.9);
		
		sim.esto(com);
		sim.addComunidad(com);
		sim.run(45);

	}
}
