

public class Ciudadano {
	
	private Comunidad comunidad;
	private int id_;
	private Enfermedad enfermedad;
	private boolean estado;

	
	Ciudadano(int i){
		this.id_ = i;
		this.estado = false;
		
	}

	public void setComunidad(Comunidad comunidad) {
		this.comunidad = comunidad;
	}	
	
	public void setid(int id) {
		this.id_ = id;
	}
	
	public void setEnfermedad(Enfermedad enfermedad) {
		this.enfermedad = enfermedad;
	}
	
	public Comunidad getComunidad() {
		return comunidad;
	}
	
	public int getid(int entero) {
		id_ = entero;
		return id_;
	}
	
	public Enfermedad getEnfermedad() {
		return enfermedad;
	}

	public void tratamiento() {
	
		
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	

}