Proyecto 1 Programacion Avanzada.
Requiere de una version de Java 11 o superior.

El proyecto se divide en las clases Main, Simulador, Enfermedad, Ciudadano y Comunidad.
Tiene por objetivo simular el comportamiento de una Enfermedad, cualesquiera sea esta,
que se desarrolla en una Comunidad la cual posee individuos llamados Ciudadanos; Algunos
de estos ciudadanos portan la enfermedad siendo estos llamados infectados.

En la clase Main se definieron los valores para las variables Enfermedad, Comunidad y Simulador

public static void main(String[] args) {
		
		Enfermedad dis = new Enfermedad();
		Comunidad com = new Comunidad();
		Simulador sim = new Simulador();
		
		dis.setprobInfeccion(0.3);
		dis.setpromPasos(18);
		
		com.setnumCiudadanos(2000);
		com.setpromConexionFisica(8);
		com.setenfermedad("enfermedad");
		com.setnumInfectados(10);
		com.setprobConexionFisica(0.9);
		
		sim.esto(com);
		sim.addComunidad(com);
		sim.run(45);

	}
Desde la linea 13 a la 15 se instancian las clases Comunidad, Enfermedad y Simulador.
Desde la linea 17 hasta la 28 se le agregan valores a los metodos de las clases ya instanciadas.

En la clase Simulador se necesito importar java.util.ArrayList y java.util.Random.
